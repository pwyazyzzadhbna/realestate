import React from "react";
import millify from "millify";
import Link from "next/link";
// icon

import { FaBed, FaBath } from "react-icons/fa";
import { BsGridFill } from "react-icons/bs";
import { GoVerified } from "react-icons/go";
//material
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";

const Styles = makeStyles((theme) => ({
  root: {
    maxWidth: 300,
    cursor: "pointer",
  },
  media: {
    width: "500px",
    height: "300px",
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

function Property({
  property: {
    coverPhoto,
    price,
    rentFrequency,
    rooms,
    title,
    baths,
    area,
    agency,
    isVerified,
    externalID,
  },
}) {
  const classes = Styles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Link href={`/property/${externalID}`} passHref>
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
          image={coverPhoto.url}
          title={area}
          width={500}
          height={300}
        />

        <CardContent className="flex items-center justify-center">
          <div className="flex flex-col mr-6">
            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              className={"flex"}
            >
              <div className={"pr-3 text-green-400"}>
                {isVerified && <GoVerified />}
              </div>
              AED {price}
              {rentFrequency && `/${rentFrequency}`}
            </Typography>
            <div className={"flex justify-between text-blue-500"}>
              <span className="flex items-center">
                | {rooms} <FaBed />{" "}
              </span>
              <span className="flex items-center ">
                | {baths}
                <FaBath />{" "}
              </span>
              <span className="flex items-center ">
                | {millify(area)} sqft <BsGridFill />
              </span>
            </div>

            <Typography variant="body2" color="textSecondary" component="p">
              {title.length > 30 ? title.substring(0, 30) + "..." : title}
            </Typography>
          </div>
          <Avatar alt="Remy Sharp" src={agency?.logo?.url} />
        </CardContent>
      </Card>
    </Link>
  );
}

export default Property;
