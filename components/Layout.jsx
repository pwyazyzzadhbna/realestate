import React from "react";
//components
import Header from "./navbar/Header";

function Layout({ children }) {
  return (
    <div>
      <header className="mb-20">
        <Header />
      </header>
      <main>{children}</main>
    </div>
  );
}

export default Layout;
