import React, { useState } from "react";
import {
  Drawer,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import { FcMenu, FcHome, FcAbout } from "react-icons/fc";
const pages = ["Products", "Services", "ABoutUs", "ContactUs"];
const DrawerComp = ({Items}) => {
  const [openDrawer, setOpenDrawer] = useState(false);
console.log("List",List)
  return (
    <React.Fragment>
      <Drawer
        anchor="left"
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
      >
        <List>
          {Items?.map((page) => (
            <ListItemButton key={page.value}>
              <ListItemIcon>
                {page.icon}
                <ListItemText>{page.label}</ListItemText>
              </ListItemIcon>
            </ListItemButton>
          ))}
        </List>
      </Drawer>
      <IconButton
        sx={{ color: "white", marginLeft: "auto" }}
        onClick={() => setOpenDrawer(!openDrawer)}
      >
        <FcMenu/>
      </IconButton>
    </React.Fragment>
  );
};

export default DrawerComp;