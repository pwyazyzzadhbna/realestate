import React, { useState } from "react";
import Link from "next/link";
// material
import {
  AppBar,
  Button,
  Tab,
  Tabs,
  Toolbar,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";

// icons
import { FcMenu, FcHome, FcAbout } from "react-icons/fc";
import { BsSearch } from "react-icons/bs";
import { FiKey } from "react-icons/fi";
// component
import DrawerComp from "./Drawer";

const Header = () => {
  const [value, setValue] = useState();
  const theme = useTheme();
  const List = [
    {
      label: "Home",
      value: "home",
      href: "/",
      icon: <FcHome className="text-2xl" />,
    },
    {
      label: "Search",
      value: "search",
      href: "/search",
      icon: <BsSearch className="text-2xl" />,
    },
    {
      label: "But Property",
      value: "but-Property",
      href: "/search?purpose=for-sale",
      icon: <FcAbout className="text-2xl" />,
    },
    {
      label: "Rent Property",
      value: "rent-property",
      href: "/search?purpose=for-rent",
      icon: <FiKey className="text-2xl" />,
    },
  ];

  console.log(theme);
  const isMatch = useMediaQuery(theme.breakpoints.down("md"));
  console.log("ismatch",isMatch);

  return (
    <>
      <AppBar>
        <Toolbar sx={{ background: "#063970", color: "#fff"}}>
          {isMatch ? (
            <>
              <Typography sx={{ fontSize: "2rem", paddingLeft: "10%" }}>
                Shoppee
              </Typography>
              <DrawerComp Items={List}/>
            </>
          ) : (
            <>
              <Tabs
                sx={{ marginLeft: "auto" }}
                indicatorColor="secondary"
                textColor="inherit"
                value={value}
                onChange={(e, value) => setValue(value)}
              >
                {List.map((val) => (
                  <Link key={val.value} href={val.href} >
                    <Tab label={val.label} />
                  </Link>
                ))}
              </Tabs>
            </>
          )}
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
