import React from "react";
import Link from "next/link";
// material
import Button from "@mui/material/Button";

function Banner({
  purpose,
  title1,
  title2,
  desc1,
  desc2,
  buttonText,
  linkName,
  imageUrl,
}) {
  return (
    <div className="flex flex-wrap justify-center py-4">
      <img src={imageUrl} width={500} height={300} />
      <div className="p-5">
        <p className="text-gray-500 text-sm">{purpose}</p>
        <p className="text-3xl bold">
          {title1}
          <br />
          {title2}
        </p>
        <p className="text-lg text-gray.700 py-3">
          {desc1}
          <br />
          {desc2}
        </p>
        <Button variant="contained" color="primary" className="bg-blue-600">
          <Link href={linkName}>
            <a>{buttonText}</a>
          </Link>
        </Button>
      </div>
    </div>
  );
}

export default Banner;
