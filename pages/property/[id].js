import React from "react";
// service
import { baseUrl, fetchApi } from "../../utils/fetchApi";

//component
import millify from "millify";

//material
import { makeStyles } from "@material-ui/core/styles";
import ImageList from "@material-ui/core/ImageList";
import ImageListItem from "@material-ui/core/ImageListItem";
import ImageListItemBar from "@material-ui/core/ImageListItemBar";
import Avatar from "@material-ui/core/Avatar";

// icon
import { FaBed, FaBath } from "react-icons/fa";
import { BsGridFill } from "react-icons/bs";
import { GoVerified } from "react-icons/go";

const Styles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: "500px",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  imageList: {
    flexWrap: "nowrap",
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: "translateZ(0)",
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
  },
}));

function property({
  propertyDetails: {
    price,
    rentFrequency,
    rooms,
    title,
    baths,
    area,
    agency,
    isVerified,
    description,
    type,
    purpose,
    furnishingStatus,
    amenities,
    photos,
  },
}) {
  const classes = Styles();

  return (
    <div className="p-4 md:p-28">
      {photos && (
        <div className={classes.root}>
          <ImageList className={classes.imageList} cols={2.5}>
            {photos.map((item) => (
              <ImageListItem key={item.img} style={{ height: "500px" }}>
                <img src={item.url} className="h-full" />
                <ImageListItemBar
                  classes={{
                    root: classes.titleBar,
                    title: classes.title,
                  }}
                />
              </ImageListItem>
            ))}
          </ImageList>
        </div>
      )}
      <div className="flex flex-col">
        <div className="flex my-4 justify-between">
          <div className="flex flex-col items-center">
            <div className={"flex items-center"}>
              {isVerified && (
                <GoVerified className={"pr-3 text-4xl text-green-400"} />
              )}
              AED {price}
              {rentFrequency && `/${rentFrequency}`}
            </div>

            <div className={"flex justify-between text-blue-500"}>
              <span className="flex items-center">
                | {rooms} <FaBed />{" "}
              </span>
              <span className="flex items-center ">
                | {baths}
                <FaBath />{" "}
              </span>
              <span className="flex items-center ">
                | {millify(area)} sqft <BsGridFill />
              </span>
            </div>
          </div>
          <Avatar alt="Remy Sharp" src={agency?.logo?.url} />
        </div>
        <h1 className="text-2xl font-bold	">{title}</h1>
        <p className="text-gray-600">{description}</p>
        <div className="w-full h-20 border-b my-2">
          <div className="w-2/5 flex justify-between">
            <span>Type</span>
            <span className="font-bold">{type}</span>
          </div>
          <div className="w-2/5 flex justify-between">
            <span>Purpose</span>
            <span className="font-bold">{purpose}</span>
          </div>
        </div>
        {furnishingStatus && (
          <div className="w-full h-20 border-b">
            <div className="w-2/5 flex justify-between">
              <span>Furnishing Status</span>
              <span className="font-bold">{furnishingStatus}</span>
            </div>
          </div>
        )}
        {amenities.length && <h1 className="text-2xl font-bold	">Facilites:</h1>}
        <div className="flex flex-wrap">
          {amenities?.map((item) =>
            item?.amenities?.map((amenity) => (
              <span
                key={amenity.text}
                className="font-bold text-blue-400 text-1xl p-4 bg-gray-200 m-1 rounded-lg"
              >
                {amenity.text}
              </span>
            ))
          )}
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps({ params: { id } }) {
  const data = await fetchApi(`${baseUrl}/properties/detail?externalID=${id}`);
  return {
    props: {
      propertyDetails: data,
    },
  };
}

export default property;
