import { useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
//icon
import { BsFilter } from "react-icons/bs";
// component
import Property from "../components/Property";
// service
import SearchFilters from "../components/SearchFilters";
import { baseUrl, fetchApi } from "../utils/fetchApi";

// img
import noresult from "../assets/images/noresult.svg";

function Search({ properties }) {
  const Router = useRouter();

  const [SearchFilter, SetSearchFilter] = useState(false);
  const handleSearchFilters = () => {
    SetSearchFilter((prevFilters) => !prevFilters);
  };
  return (
    <div className="flex flex-col justify-center">
      <div
        className="flex justify-center cursor-pointer bg-gray-100 p-2 font-black text-lg border-b"
        onClick={handleSearchFilters}
      >
        <p>Search Property By Filters</p>
        <BsFilter className="pl-2 w-7" />
      </div>
      {SearchFilter && <SearchFilters />}
      <p className="text-2xl p-4 font-bold">Property {Router.query.purpose}</p>
      <div className="flex flex-wrap justify-between">
        {properties.map((property) => (
          <div key={property.id} className="m-2">
            <Property property={property} />
          </div>
        ))}
      </div>
      {properties.length === 0 && (
        <div className="flex justify-center flex-col my-5 ">
          <Image alt="no Result" src={noresult} />
        </div>
      )}
    </div>
  );
}

export async function getServerSideProps({ query }) {
  const purpose = query.purpose || "for-rent";
  const rentFrequency = query.rentFrequency || "yearly";
  const minPrice = query.minPrice || "0";
  const maxPrice = query.maxPrice || "1000000";
  const roomsMin = query.roomsMin || "0";
  const bathsMin = query.bathsMin || "0";
  const sort = query.sort || "price-desc";
  const areaMax = query.areaMax || "35000";
  const locationExternalIDs = query.locationExternalIDs || "5002";
  const categoryExternalID = query.categoryExternalID || "4";
  const data = await fetchApi(
    `${baseUrl}/properties/list?locationExternalIDs=${locationExternalIDs}&purpose=${purpose}&categoryExternalID=${categoryExternalID}&bathsMin=${bathsMin}&rentFrequency=${rentFrequency}&priceMin=${minPrice}&priceMax=${maxPrice}&roomsMin=${roomsMin}&sort=${sort}&areaMax=${areaMax}`
  );

  return {
    props: {
      properties: data?.hits,
    },
  };
}

export default Search;
